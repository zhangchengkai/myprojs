#include "Common.hlsli"

float4 main( float3 pos : POSITION ) : SV_POSITION
{
	// Transform to world space.
	float4 posW = mul(float4(pos, 1.0f), gWorld);

	// Transform to homogeneous clip space.
	float4 posH = mul(posW, gViewProj);

	return posH;
}