#include "Debug.hlsli"

float4 PS(VertexOut pin) : SV_Target
{
	return float4(gSsaoMap.Sample(gsamLinearWrap, pin.TexC).rrr, 1.0f);
}