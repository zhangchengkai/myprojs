#pragma once
namespace gConfig
{
	const size_t MaxBoneTransformCount = 96;
	const size_t SsaoOffsetVecCount = 14;
}
