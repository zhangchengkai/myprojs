//***************************************************************************************
// GameTimer.h by Frank Luna (C) 2011 All Rights Reserved.
//***************************************************************************************

#ifndef GAMETIMER_H
#define GAMETIMER_H

class GameTimer
{
public:
	GameTimer();

	// Return the time elapsed from Reset() to last tick event.
	float TotalTime()const; // in seconds (without paused time)

	// Return the time elapsed between last two Tick().
	// Note that before first Tick(), this func returns -1,
	// after first Tick(), this func returns the time between Reset() and Tick(),
	// after Tick() in stopping time, this func returns 0,
	// after Start() and the first Tick(), this func returns the time between Start() and Tick().
	float DeltaTime()const; // in seconds (without paused time)

	// These four methods are all tick events.
	void Reset(); // Call before message loop.
	void Start(); // Call when unpaused.
	void Stop();  // Call when paused.
	void Tick();  // Call every frame.

#if defined(_DEBUG)
	void LogInfo() const;
#endif
private:
	double mSecondsPerCount;
	double mDeltaTime;

	__int64 mBaseTime;
	__int64 mPausedTime;
	__int64 mStopTime;
	__int64 mPrevTime;
	__int64 mCurrTime;

	bool mStopped;
};

#endif // GAMETIMER_H